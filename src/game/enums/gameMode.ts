export enum GameMode {
  aram = "ARAM",
  ascension = "ODIN",
  classic = "CLASSIC",
  tutorial = "TUTORIAL",
}
