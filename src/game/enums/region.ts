export enum Region {
  brazil = "BR1",
  eune = "EUN1",
  euw = "EUW1",
  jp = "JP1",
  kr = "KR",
  lan = "LA1",
  las = "LA2",
  na = "NA1",
  oce = "OC1",
  pbe = "PBE",
  ru = "RU1",
  tr = "TR1",
}
