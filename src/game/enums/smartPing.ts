export enum SmartPing {
  assistMe = 6,
  basic = 0,
  caution = 5,
  danger = 2,
  mia = 3,
  onMyWay = 4,
}
