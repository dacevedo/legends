export enum Tier {
  bronze = "BRONZE",
  challenger = "CHALLENGER",
  diamond = "DIAMOND",
  gold = "GOLD",
  master = "MASTER",
  platinum = "PLATINUM",
  silver = "SILVER",
}
