import { Channel } from "../../protocol/enums/channel";
import { Command } from "../../protocol/enums/command";
import { send } from "../../protocol/packets";
import { announcement } from "../../protocol/packets/structs/announcement";
import type { Announcement } from "../enums/announcement";
import type { IPlayerState } from "../state/types";

const announce = (
  messageId: Announcement,
  netId: number,
  player: IPlayerState
): void => {
  send(
    Channel.serverToClient,
    announcement({
      command: Command.announcement,
      messageId,
      netId,
    }),
    player
  );
};

export { announce };
