import { Channel } from "../../protocol/enums/channel";
import { Command } from "../../protocol/enums/command";
import { broadcast } from "../../protocol/packets";
import { startGame } from "../../protocol/packets/structs/startGame";
import { matchState } from "../state/match";
import type { Dispatch, State } from "../state/store";

const start = (dispatch: Dispatch, state: State): void => {
  dispatch(matchState.actions.start());
  broadcast(
    Channel.serverToClient,
    startGame({
      command: Command.startGameResponse,
      enablePause: Number(state.match.enablePause),
    }),
    state.players
  );
};

export { start };
