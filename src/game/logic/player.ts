import { announce } from "./announcer";
import { start } from "./match";

import { Channel } from "../../protocol/enums/channel";
import { Command } from "../../protocol/enums/command";
import { send } from "../../protocol/packets";
import { keyCheckResponse } from "../../protocol/packets/structs/keyCheckResponse";
import { startGame } from "../../protocol/packets/structs/startGame";
import { serializeArray } from "../../utils/serialization";
import { Announcement } from "../enums/announcement";
import { playersState } from "../state/players";
import type { Dispatch, State } from "../state/store";
import type { IPlayerState } from "../state/types";

const connect = (
  dispatch: Dispatch,
  partialKey: number[],
  peerPlayer: IPlayerState,
  state: State
): void => {
  if (peerPlayer.peer) {
    dispatch(
      playersState.actions.connect({
        peer: peerPlayer.peer,
        playerId: peerPlayer.playerId,
      })
    );
    state.players.forEach((player) => {
      send(
        Channel.handshake,
        keyCheckResponse({
          clientId: player.clientId,
          command: Command.keyCheck,
          partialKey: serializeArray(partialKey),
          playerId: player.playerId,
        }),
        peerPlayer
      );
    });
  }
};

const disconnect = (
  dispatch: Dispatch,
  peerPlayer: IPlayerState,
  state: State
): void => {
  dispatch(playersState.actions.disconnect({ playerId: peerPlayer.playerId }));

  if (state.match.started) {
    state.players.forEach((player) => {
      if (
        player.teamId === peerPlayer.teamId &&
        player.playerId !== peerPlayer.playerId
      ) {
        announce(
          Announcement.playerDisconnected,
          peerPlayer.character.netId,
          player
        );
      }
    });
  } else {
    const allRemainingLoaded = state.players
      .filter((player) => player.peer)
      .every((player) => player.loaded);

    if (allRemainingLoaded) {
      start(dispatch, state);
    }
  }
};

const reconnect = (peerPlayer: IPlayerState, state: State): void => {
  state.players
    .filter(
      (player) =>
        player.playerId !== peerPlayer.playerId &&
        player.teamId === peerPlayer.teamId
    )
    .forEach((player) => {
      announce(
        Announcement.playerReconnected,
        peerPlayer.character.netId,
        player
      );
    });

  send(
    Channel.serverToClient,
    startGame({
      command: Command.startGameResponse,
      enablePause: Number(state.match.enablePause),
    }),
    peerPlayer
  );
};

export { connect, disconnect, reconnect };
