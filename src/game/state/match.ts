import { createSlice } from "@reduxjs/toolkit";

import type { IMatchState } from "./types";

import { GameMap } from "../enums/gameMap";
import { GameMode } from "../enums/gameMode";
import { Region } from "../enums/region";

const initialState: IMatchState = {
  enablePause: true,
  gameMap: GameMap.howlingAbyss,
  gameMode: GameMode.classic,
  nextNetId: 1073741836,
  region: Region.lan,
  started: false,
};

export const matchState = createSlice({
  initialState,
  name: "match",
  reducers: {
    start: (match) => ({ ...match, started: true }),
  },
});
