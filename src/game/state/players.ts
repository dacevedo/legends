import type { PayloadAction } from "@reduxjs/toolkit";
import { createSlice } from "@reduxjs/toolkit";
import type { IENetPeer } from "enet-js";

import type { IPlayerState } from "./types";

import { characters } from "../characters";
import { Spell } from "../enums/spell";
import { Team } from "../enums/team";
import { Tier } from "../enums/tier";

const initialState: IPlayerState[] = [
  {
    character: {
      name: characters.theVoidWalker.name,
      netId: 1073741824,
      skin: characters.theVoidWalker.skins.harbinger,
    },
    clientId: 0,
    encryptionKey: "17BLOhi6KZsTtldTsizvHg==",
    icon: 659,
    level: 30,
    loaded: false,
    name: "Player 1",
    peer: null,
    playerId: 1,
    ribbon: 2,
    spell1: Spell.ignite,
    spell2: Spell.flash,
    teamId: Team.blue,
    tier: Tier.diamond,
    wardSkin: 0,
  },
];

export const playersState = createSlice({
  initialState,
  name: "players",
  reducers: {
    connect: (
      players,
      action: PayloadAction<{ peer: IENetPeer; playerId: number }>
    ) =>
      players.map((player) => {
        if (player.playerId === action.payload.playerId) {
          return { ...player, peer: action.payload.peer };
        }

        return player;
      }),
    disconnect: (players, action: PayloadAction<{ playerId: number }>) =>
      players.map((player) => {
        if (player.playerId === action.payload.playerId) {
          return { ...player, loaded: false, peer: null };
        }

        return player;
      }),
    setLoaded: (players, action: PayloadAction<{ playerId: number }>) =>
      players.map((player) => {
        if (player.playerId === action.payload.playerId) {
          return { ...player, loaded: true };
        }

        return player;
      }),
  },
});
