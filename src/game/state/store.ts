/* eslint-disable @typescript-eslint/no-type-alias */
import { configureStore } from "@reduxjs/toolkit";

import { matchState } from "./match";
import { playersState } from "./players";

const store = configureStore({
  devTools: false,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({ serializableCheck: false }),
  reducer: {
    match: matchState.reducer,
    players: playersState.reducer,
  },
});

type State = ReturnType<typeof store.getState>;
type Dispatch = typeof store.dispatch;

export type { Dispatch, State };
export { store };
