import type { IENetPeer } from "enet-js";

import type { GameMap } from "../enums/gameMap";
import type { Region } from "../enums/region";
import type { Spell } from "../enums/spell";
import type { Team } from "../enums/team";
import type { Tier } from "../enums/tier";

interface ICharacterState {
  name: string;
  netId: number;
  skin: number;
}

interface IPlayerState {
  character: ICharacterState;
  clientId: number;
  encryptionKey: string;
  icon: number;
  level: number;
  loaded: boolean;
  name: string;
  peer: IENetPeer | null;
  playerId: number;
  ribbon: number;
  spell1: Spell;
  spell2: Spell;
  teamId: Team;
  tier: Tier;
  wardSkin: number;
}

interface IMatchState {
  enablePause: boolean;
  gameMap: GameMap;
  gameMode: string;
  nextNetId: number;
  region: Region;
  started: boolean;
}

export type { IMatchState, IPlayerState };
