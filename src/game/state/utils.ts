import type { IENetPeer } from "enet-js";

import type { State } from "./store";
import type { IPlayerState } from "./types";

const getPlayerById = (
  playerId: number,
  state: State
): IPlayerState | undefined => {
  return state.players.find((player) => player.playerId === playerId);
};

const getPlayerByPeer = (
  peer: IENetPeer,
  state: State
): IPlayerState | undefined => {
  return state.players.find(
    (player) =>
      player.peer &&
      player.peer.address.host === peer.address.host &&
      player.peer.address.port === peer.address.port
  );
};

export { getPlayerById, getPlayerByPeer };
