import type { IENetEvent, IENetHost, IENetPacket, IENetPeer } from "enet-js";
import { ENetEventType, ENetPacketFlag, enet } from "enet-js";
import type {
  enetEvent,
  enetPacket,
  enetPeer,
} from "enet-js/dist/native/structs";
import { enetHost } from "enet-js/dist/native/structs";
import type { Pointer } from "ref-napi";

import * as handlers from "./protocol/handlers";

jest.mock("enet-js", () => {
  const actualEnet = jest.requireActual<Record<string, unknown>>("enet-js");

  return {
    ...actualEnet,
    enet: {
      host: {
        create: jest.fn(),
        service: jest.fn(),
      },
      initialize: jest.fn().mockReturnValue(0),
      packet: {
        destroy: jest.fn(),
      },
    },
  };
});

const main = (): void => {
  // eslint-disable-next-line @typescript-eslint/no-require-imports
  require("./main");
};

describe("main", () => {
  // eslint-disable-next-line jest/no-hooks
  beforeAll(() => {
    jest.useFakeTimers();
  });

  // eslint-disable-next-line jest/no-hooks
  afterAll(() => {
    jest.useRealTimers();
  });

  it("should start the network loop", () => {
    expect.hasAssertions();

    jest.spyOn(global, "setInterval");
    (enet.host.create as jest.Mock<IENetHost>).mockReturnValueOnce({
      native: enetHost().ref(),
    });
    main();

    expect(setInterval).toHaveBeenCalledTimes(1);
  });

  it("should process network events", () => {
    expect.hasAssertions();

    const mockedPacketData = Buffer.from("Test");
    const mockedPacket: IENetPacket = {
      data: mockedPacketData,
      dataLength: mockedPacketData.length,
      flags: ENetPacketFlag.none,
      native: Buffer.from("") as Pointer<ReturnType<typeof enetPacket>>,
      referenceCount: 0,
    };
    const mockedPeer: IENetPeer = {
      address: { host: "", port: 0 },
      mtu: 0,
      native: Buffer.from("") as Pointer<ReturnType<typeof enetPeer>>,
    };
    const mockedEventNative = Buffer.from("") as Pointer<
      ReturnType<typeof enetEvent>
    >;

    (enet.host.service as jest.Mock<IENetEvent>)
      .mockReturnValueOnce({
        channelID: -1,
        data: 0,
        native: mockedEventNative,
        packet: null,
        peer: null,
        type: ENetEventType.none,
      })
      .mockReturnValueOnce({
        channelID: -1,
        data: 0,
        native: mockedEventNative,
        peer: mockedPeer,
        type: ENetEventType.connect,
      })
      .mockReturnValueOnce({
        channelID: -1,
        data: 0,
        native: mockedEventNative,
        peer: mockedPeer,
        type: ENetEventType.disconnect,
      })
      .mockReturnValueOnce({
        channelID: -1,
        data: 0,
        native: mockedEventNative,
        packet: mockedPacket,
        peer: mockedPeer,
        type: ENetEventType.receive,
      });
    main();

    jest.spyOn(handlers, "handle");
    jest.advanceTimersToNextTimer(4);

    expect(handlers.handle).toHaveBeenCalledTimes(1);
  });
});
