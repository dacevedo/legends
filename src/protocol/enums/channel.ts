export enum Channel {
  clientToServer = 1,
  communication = 5,
  handshake = 0,
  loadingScreen = 7,
  lowPriority = 4,
  serverToClient = 3,
}
