import { Channel } from "../../enums/channel";
import { Command } from "../../enums/command";
import { broadcast } from "../../packets";
import { characterEmote } from "../../packets/structs/characterEmote";
import type { ICommandHandlerArgs } from "../types";

export const handleCharacterEmote = ({
  data,
  state,
}: ICommandHandlerArgs): void => {
  const { netId, emoteId } = characterEmote(data);
  // Pending to filter according to vision once implemented
  broadcast(
    Channel.serverToClient,
    characterEmote({
      command: Command.characterEmoteResponse,
      emoteId,
      netId,
    }),
    state.players
  );
};
