import { Team } from "../../../game/enums/team";
import type { State } from "../../../game/state/store";
import type { IPlayerState } from "../../../game/state/types";
import { serializeString } from "../../../utils/serialization";
import { Channel } from "../../enums/channel";
import { Command } from "../../enums/command";
import { send } from "../../packets";
import { characterSpawn } from "../../packets/structs/characterSpawn";
import { endSpawn } from "../../packets/structs/endSpawn";
import { enterVisibilityClient } from "../../packets/structs/enterVisiblityClient";
import { playerInfo } from "../../packets/structs/playerInfo";
import { startSpawn } from "../../packets/structs/startSpawn";
import type { ICommandHandlerArgs } from "../types";

const getTeamNumber = (teamId: Team): number => {
  if (teamId === Team.blue) {
    return 1;
  }

  return 0;
};

const spawnCharacters = (peerPlayer: IPlayerState, state: State): void => {
  state.players
    .filter((player) => player.clientId > -1)
    .forEach((player) => {
      send(
        Channel.serverToClient,
        characterSpawn({
          characterName: serializeString(player.character.name),
          clientId: player.clientId,
          command: Command.characterSpawn,
          netId: player.character.netId,
          // Intentional duplicate
          netId2: player.character.netId,
          netNodeId: 40,
          playerName: serializeString(player.name),
          skinId: player.character.skin,
          teamNumber: getTeamNumber(player.teamId),
        }),
        peerPlayer
      );

      send(
        Channel.serverToClient,
        playerInfo({
          command: Command.playerInfo,
          level: player.level,
          netId: player.character.netId,
          spell1: player.spell1,
          spell2: player.spell2,
          wardSkin: player.wardSkin,
        }),
        peerPlayer
      );

      send(
        Channel.serverToClient,
        enterVisibilityClient({
          command: Command.enterVisibilityClient,
          netId: player.character.netId,
        }),
        peerPlayer
      );
    });
};

export const handleCharacterLoaded = ({
  peerPlayer,
  state,
}: ICommandHandlerArgs): void => {
  send(
    Channel.serverToClient,
    startSpawn({ command: Command.startSpawn }),
    peerPlayer
  );

  spawnCharacters(peerPlayer, state);

  send(
    Channel.serverToClient,
    endSpawn({ command: Command.endSpawn }),
    peerPlayer
  );
};
