import { disconnect } from "../../../game/logic/player";
import type { ICommandHandlerArgs } from "../types";

export const handleExit = ({
  dispatch,
  peerPlayer,
  state,
}: ICommandHandlerArgs): void => {
  disconnect(dispatch, peerPlayer, state);
};
