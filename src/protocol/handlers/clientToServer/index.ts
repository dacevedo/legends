import { handleCharacterEmote } from "./characterEmote";
import { handleCharacterLoaded } from "./characterLoaded";
import { handleExit } from "./exit";
import { handleLockCamera } from "./lockCamera";
import { handleMovement } from "./movement";
import { handlePingLoadInfo } from "./pingLoadInfo";
import { handleQueryStatus } from "./queryStatus";
import { handleSmartPing } from "./smartPing";
import { handleStartGame } from "./startGame";
import { handleSyncVersion } from "./syncVersion";
import { handleView } from "./view";

import { getPlayerByPeer } from "../../../game/state/utils";
import { Command } from "../../enums/command";
import { getData } from "../../packets";
import type { IChannelHandlerArgs, ICommandHandlerArgs } from "../types";

const handlers: Record<number, (args: ICommandHandlerArgs) => void> = {
  [Command.characterEmoteRequest]: handleCharacterEmote,
  [Command.characterLoaded]: handleCharacterLoaded,
  [Command.exit]: handleExit,
  [Command.lockCamera]: handleLockCamera,
  [Command.movementConfirm]: () => null,
  [Command.movementRequest]: handleMovement,
  [Command.pingLoadInfoRequest]: handlePingLoadInfo,
  [Command.queryStatusRequest]: handleQueryStatus,
  [Command.smartPingRequest]: handleSmartPing,
  [Command.startGameRequest]: handleStartGame,
  [Command.syncVersionRequest]: handleSyncVersion,
  [Command.viewRequest]: handleView,
};

export const handleClientToServer = ({
  dispatch,
  packet,
  peer,
  state,
}: IChannelHandlerArgs): void => {
  const peerPlayer = getPlayerByPeer(peer, state);

  if (peerPlayer) {
    const data = getData(packet, peerPlayer.encryptionKey);
    const command = data.readUInt8(0);

    if (command in handlers) {
      console.log(">> [CLIENTTOSERVER]", { command: Command[command] });
      handlers[command]({ data, dispatch, packet, peer, peerPlayer, state });
    } else {
      console.error(">> [CLIENTTOSERVER] Unhandled command:", command);
    }
  } else {
    console.error(
      ">> [CLIENTTOSERVER] Couldn't find player for peer",
      peer.address
    );
  }
};
