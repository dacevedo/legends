import { performance } from "perf_hooks";

import { Movement } from "../../../game/enums/movement";
import { serializeArray } from "../../../utils/serialization";
import { Channel } from "../../enums/channel";
import { Command } from "../../enums/command";
import { send } from "../../packets";
import { movementRequest } from "../../packets/structs/movementRequest";
import { movementResponse } from "../../packets/structs/movementResponse";
import type { ICommandHandlerArgs } from "../types";

export const handleMovement = ({
  data,
  peerPlayer,
  state,
}: ICommandHandlerArgs): void => {
  const { netId, type, x, z, targetNetId, bitField } = movementRequest(data);
  console.log(peerPlayer.name, targetNetId, "Moving to", { x, z });

  // Intentional usage
  // eslint-disable-next-line no-bitwise
  const waypointsLength = bitField >> 1;

  switch (type) {
    case Movement.move:
      state.players.forEach((player) => {
        send(
          Channel.lowPriority,
          movementResponse({
            command: Command.movementResponse,
            count: waypointsLength,
            netId,
            syncId: performance.now(),
            // Sending as received, pending to process properly
            // eslint-disable-next-line @typescript-eslint/no-magic-numbers
            unknown: serializeArray(Array.from(data.slice(18))),
          }),
          player
        );
      });
      break;

    default:
      console.error("Unhandled movement type", type);
      break;
  }
};
