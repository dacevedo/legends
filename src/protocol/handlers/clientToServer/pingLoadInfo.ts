import { Channel } from "../../enums/channel";
import { Command } from "../../enums/command";
import { broadcast } from "../../packets";
import { pingLoadInfo } from "../../packets/structs/pingLoadInfo";
import type { ICommandHandlerArgs } from "../types";

export const handlePingLoadInfo = ({
  data,
  peerPlayer,
  state,
}: ICommandHandlerArgs): void => {
  const {
    loadedPercentage,
    netId,
    ping,
    ready,
    secondsElapsed,
    secondsRemaining,
  } = pingLoadInfo(data);

  broadcast(
    Channel.lowPriority,
    pingLoadInfo({
      clientId: peerPlayer.clientId,
      command: Command.pingLoadInfoResponse,
      loadedPercentage,
      netId,
      ping,
      playerId: peerPlayer.playerId,
      ready,
      secondsElapsed,
      secondsRemaining,
    }),
    state.players
  );
};
