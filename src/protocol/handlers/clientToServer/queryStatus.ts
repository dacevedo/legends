import { Channel } from "../../enums/channel";
import { Command } from "../../enums/command";
import { send } from "../../packets";
import { queryStatusResponse } from "../../packets/structs/queryStatusResponse";
import type { ICommandHandlerArgs } from "../types";

export const handleQueryStatus = ({
  peerPlayer,
}: ICommandHandlerArgs): void => {
  send(
    Channel.serverToClient,
    queryStatusResponse({
      command: Command.queryStatusResponse,
      gameStatus: 1,
    }),
    peerPlayer
  );
};
