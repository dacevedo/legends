import { SmartPing } from "../../../game/enums/smartPing";
import { Channel } from "../../enums/channel";
import { Command } from "../../enums/command";
import { broadcastTeam } from "../../packets";
import { smartPingRequest } from "../../packets/structs/smartPingRequest";
import { smartPingResponse } from "../../packets/structs/smartPingResponse";
import type { ICommandHandlerArgs } from "../types";

export const handleSmartPing = ({
  data,
  peerPlayer,
  state,
}: ICommandHandlerArgs): void => {
  const { netId, x, y, targetNetId, type } = smartPingRequest(data);
  console.log("PING", {
    netId,
    targetNetId,
    type: SmartPing[type],
    x,
    y,
  });

  broadcastTeam(
    Channel.serverToClient,
    smartPingResponse({
      command: Command.smartPingResponse,
      // Unknown at the moment, seems to be the result of bitwise operations
      features: 251,
      netId,
      sourceNetId: peerPlayer.character.netId,
      targetNetId,
      type,
      x,
      y,
    }),
    state.players,
    peerPlayer.teamId
  );
};
