import { start } from "../../../game/logic/match";
import { reconnect } from "../../../game/logic/player";
import { playersState } from "../../../game/state/players";
import type { ICommandHandlerArgs } from "../types";

export const handleStartGame = ({
  dispatch,
  peerPlayer,
  state,
}: ICommandHandlerArgs): void => {
  if (state.match.started) {
    reconnect(peerPlayer, state);
  } else {
    dispatch(playersState.actions.setLoaded({ playerId: peerPlayer.playerId }));

    const loadedPlayers = state.players.filter(
      (player) => player.peer && player.loaded
    );

    if (loadedPlayers.length + 1 === state.players.length) {
      start(dispatch, state);
    }
  }
};
