import { serializeArray, serializeString } from "../../../utils/serialization";
import { SERVER_VERSION } from "../../constants";
import { Channel } from "../../enums/channel";
import { Command } from "../../enums/command";
import { send } from "../../packets";
import { syncVersionRequest } from "../../packets/structs/syncVersionRequest";
import { syncVersionResponse } from "../../packets/structs/syncVersionResponse";
import type { ICommandHandlerArgs } from "../types";

export const handleSyncVersion = ({
  data,
  peerPlayer,
  state,
}: ICommandHandlerArgs): void => {
  const { netId, version } = syncVersionRequest(data);

  const clientVersion = version.buffer.toString(
    "utf-8",
    0,
    version.buffer.indexOf("\0")
  );

  send(
    Channel.serverToClient,
    syncVersionResponse({
      command: Command.syncVersionResponse,
      // Unknown at the moment, seems to be the result of bitwise operations
      gameFeatures: 487826,
      gameMode: serializeString(state.match.gameMode),
      hasValidVersion: clientVersion === SERVER_VERSION,
      mapId: state.match.gameMap,
      netId,
      players: serializeArray(
        state.players.map((player) => ({
          icon: player.icon,
          id: player.playerId,
          level: player.level,
          ribbon: player.ribbon,
          spell1: player.spell1,
          spell2: player.spell2,
          teamId: player.teamId,
          tier: serializeString(player.tier),
        }))
      ),
      region: serializeString(state.match.region),
      version: serializeString(SERVER_VERSION),
    }),
    peerPlayer
  );
};
