import { Channel } from "../../enums/channel";
import { Command } from "../../enums/command";
import { send } from "../../packets";
import { viewRequest } from "../../packets/structs/viewRequest";
import { viewResponse } from "../../packets/structs/viewResponse";
import type { ICommandHandlerArgs } from "../types";

export const handleView = ({ data, peerPlayer }: ICommandHandlerArgs): void => {
  const { syncId } = viewRequest(data);

  send(
    Channel.serverToClient,
    viewResponse({ command: Command.viewResponse, syncId }),
    peerPlayer
  );
};
