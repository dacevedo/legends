import { ChatChannel } from "../../../game/enums/chatChannel";
import { Channel } from "../../enums/channel";
import { broadcast, broadcastTeam } from "../../packets";
import { chatMessage } from "../../packets/structs/chatMessage";
import type { ICommandHandlerArgs } from "../types";

export const handleChatMessage = ({
  data,
  peerPlayer,
  state,
}: ICommandHandlerArgs): void => {
  const { channel } = chatMessage(data);

  const text = data.slice(chatMessage.size);
  console.log("MSG:", {
    channel: ChatChannel[channel],
    player: peerPlayer.name,
    text: text.toString("utf-8", 0, text.indexOf("\0")),
  });

  switch (channel) {
    case ChatChannel.all:
      broadcast(Channel.communication, chatMessage(data), state.players);
      break;

    case ChatChannel.team:
      broadcastTeam(
        Channel.communication,
        chatMessage(data),
        state.players,
        peerPlayer.teamId
      );
      break;

    default:
      console.error("Unhandled chat channel:", channel);
      break;
  }
};
