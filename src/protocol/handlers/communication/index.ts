import { handleChatMessage } from "./chatMessage";

import { getPlayerByPeer } from "../../../game/state/utils";
import { Command } from "../../enums/command";
import { getData } from "../../packets";
import type { IChannelHandlerArgs, ICommandHandlerArgs } from "../types";

const handlers: Record<number, (args: ICommandHandlerArgs) => void> = {
  [Command.chatMessage]: handleChatMessage,
};

export const handleCommunication = ({
  dispatch,
  packet,
  peer,
  state,
}: IChannelHandlerArgs): void => {
  const peerPlayer = getPlayerByPeer(peer, state);

  if (peerPlayer) {
    const data = getData(packet, peerPlayer.encryptionKey);
    const command = data.readUInt8(0);

    if (command in handlers) {
      console.log(">> [COMMUNICATION]", { command: Command[command] });
      handlers[command]({ data, dispatch, packet, peer, peerPlayer, state });
    } else {
      console.error(">> [COMMUNICATION] Unhandled command:", command);
    }
  } else {
    console.error(
      ">> [COMMUNICATION] Couldn't find player for peer",
      peer.address
    );
  }
};
