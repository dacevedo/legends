import { handleKeyCheck } from "./keyCheck";

import { Command } from "../../enums/command";
import { getDataUnencrypted } from "../../packets";
import type { IBaseCommandHandlerArgs, IChannelHandlerArgs } from "../types";

const handlers: Record<number, (args: IBaseCommandHandlerArgs) => void> = {
  [Command.keyCheck]: handleKeyCheck,
};

export const handleHandshake = ({
  dispatch,
  packet,
  peer,
  state,
}: IChannelHandlerArgs): void => {
  const data = getDataUnencrypted(packet);
  const command = data.readUInt8(0);

  if (command in handlers) {
    console.log(">> [HANDSHAKE]", { command: Command[command] });
    handlers[command]({ data, dispatch, packet, peer, state });
  } else {
    console.error(">> [HANDSHAKE] Unhandled command:", command);
  }
};
