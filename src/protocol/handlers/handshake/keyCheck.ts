import ref from "ref-napi";

import { connect } from "../../../game/logic/player";
import { getPlayerById } from "../../../game/state/utils";
import { keyCheckRequest } from "../../packets/structs/keyCheckRequest";
import { blowfishDecrypt } from "../../security/blowfish";
import type { IBaseCommandHandlerArgs } from "../types";

const readChecksum = (checksum: string, encryptionKey: string): number => {
  const checksumData = Buffer.alloc(ref.types.int64.size);
  checksumData.writeBigInt64LE(BigInt(checksum));

  const decrypted = blowfishDecrypt(
    checksumData,
    Buffer.from(encryptionKey, "base64")
  );

  return Number(decrypted.readBigInt64LE());
};

export const handleKeyCheck = ({
  data,
  dispatch,
  peer,
  state,
}: IBaseCommandHandlerArgs): void => {
  const { checksum, partialKey, playerId } = keyCheckRequest(data);
  const peerPlayer = getPlayerById(playerId as number, state);

  if (peerPlayer) {
    const checksumPlayerId = readChecksum(
      checksum.toString(),
      peerPlayer.encryptionKey
    );

    if (checksumPlayerId === playerId) {
      if (peerPlayer.peer) {
        console.warn("Player", playerId, "attempted to connect a second time.");
      } else {
        connect(dispatch, partialKey.toArray(), { ...peerPlayer, peer }, state);
      }
    } else {
      console.warn("Player", playerId, "provided an invalid key.");
    }
  } else {
    console.warn("Player", playerId, "not found.");
  }
};
