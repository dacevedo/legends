import type { AnyAction, ThunkAction } from "@reduxjs/toolkit";
import type { IENetPacket, IENetPeer } from "enet-js";

import { handleClientToServer } from "./clientToServer";
import { handleCommunication } from "./communication";
import { handleHandshake } from "./handshake";
import { handleLoadingScreen } from "./loadingScreen";
import type { IChannelHandlerArgs } from "./types";

import type { State } from "../../game/state/store";
import { Channel } from "../enums/channel";

const handlers: Record<number, (args: IChannelHandlerArgs) => void> = {
  [Channel.handshake]: handleHandshake,
  [Channel.clientToServer]: handleClientToServer,
  [Channel.communication]: handleCommunication,
  [Channel.loadingScreen]: handleLoadingScreen,
};

export const handle = (
  channel: Channel,
  packet: IENetPacket,
  peer: IENetPeer
): ThunkAction<void, State, undefined, AnyAction> => {
  return (dispatch, getState): void => {
    const state = getState();

    if (channel in handlers) {
      handlers[channel]({ dispatch, packet, peer, state });
    } else {
      console.error("Packet received from unhandled channel", channel);
    }
  };
};
