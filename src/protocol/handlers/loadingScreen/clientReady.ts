import { Team } from "../../../game/enums/team";
import { serializeArray, serializeString } from "../../../utils/serialization";
import { MAX_PLAYERS } from "../../constants";
import { Channel } from "../../enums/channel";
import { Command } from "../../enums/command";
import { send } from "../../packets";
import { loadScreenInfo } from "../../packets/structs/loadScreenInfo";
import { loadScreenPlayerCharacter } from "../../packets/structs/loadScreenPlayerCharacter";
import { loadScreenPlayerName } from "../../packets/structs/loadScreenPlayerName";
import type { ICommandHandlerArgs } from "../types";

export const handleClientReady = ({
  peerPlayer,
  state,
}: ICommandHandlerArgs): void => {
  const blueTeamIds = state.players
    .filter((player) => player.teamId === Team.blue)
    .map((player) => player.playerId);
  const redTeamIds = state.players
    .filter((player) => player.teamId === Team.red)
    .map((player) => player.playerId);

  send(
    Channel.loadingScreen,
    loadScreenInfo({
      blueIds: serializeArray(blueTeamIds),
      command: Command.loadScreenInfo,
      maxPlayersBlue: MAX_PLAYERS - redTeamIds.length,
      maxPlayersRed: MAX_PLAYERS - blueTeamIds.length,
      redIds: serializeArray(redTeamIds),
      totalPlayersBlue: blueTeamIds.length,
      totalPlayersRed: redTeamIds.length,
    }),
    peerPlayer
  );

  state.players.forEach((player) => {
    send(
      Channel.loadingScreen,
      loadScreenPlayerName({
        command: Command.loadName,
        length: player.name.length + 1,
        name: serializeString(player.name),
        playerId: player.playerId,
      }),
      peerPlayer
    );
    send(
      Channel.loadingScreen,
      loadScreenPlayerCharacter({
        command: Command.loadChampion,
        length: player.character.name.length + 1,
        name: serializeString(player.character.name),
        playerId: player.playerId,
        skinId: player.character.skin,
      }),
      peerPlayer
    );
  });
};
