import { handleClientReady } from "./clientReady";

import { getPlayerByPeer } from "../../../game/state/utils";
import { Command } from "../../enums/command";
import { getData } from "../../packets";
import type { IChannelHandlerArgs, ICommandHandlerArgs } from "../types";

const handlers: Record<number, (args: ICommandHandlerArgs) => void> = {
  [Command.clientReady]: handleClientReady,
};

export const handleLoadingScreen = ({
  dispatch,
  packet,
  peer,
  state,
}: IChannelHandlerArgs): void => {
  const peerPlayer = getPlayerByPeer(peer, state);

  if (peerPlayer) {
    const data = getData(packet, peerPlayer.encryptionKey);
    const command = data.readUInt8(0);

    if (command in handlers) {
      console.log(">> [LOADINGSCREEN]", { command: Command[command] });
      handlers[command]({ data, dispatch, packet, peer, peerPlayer, state });
    } else {
      console.error(">> [LOADINGSCREEN] Unhandled command:", command);
    }
  } else {
    console.error(
      ">> [LOADINGSCREEN] Couldn't find player for peer",
      peer.address
    );
  }
};
