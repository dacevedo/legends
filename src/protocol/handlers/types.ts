import type { IENetPacket, IENetPeer } from "enet-js";

import type { Dispatch, State } from "../../game/state/store";
import type { IPlayerState } from "../../game/state/types";

interface IChannelHandlerArgs {
  dispatch: Dispatch;
  packet: IENetPacket;
  peer: IENetPeer;
  state: State;
}

interface IBaseCommandHandlerArgs extends IChannelHandlerArgs {
  data: Buffer;
}

interface ICommandHandlerArgs extends IBaseCommandHandlerArgs {
  peerPlayer: IPlayerState;
}

export type {
  IBaseCommandHandlerArgs,
  IChannelHandlerArgs,
  ICommandHandlerArgs,
};
