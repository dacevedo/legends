import { enet } from "enet-js";
import type { IENetPacket, IENetPeer } from "enet-js";
import type { StructObjectBase } from "ref-struct-di";

import type { Team } from "../../game/enums/team";
import type { IPlayerState } from "../../game/state/types";
import { Channel } from "../enums/channel";
import { Command } from "../enums/command";
import { blowfishDecrypt, blowfishEncrypt } from "../security/blowfish";

const MINIMUM_ENCRYPTED_LENGTH = 8;

const createPacket = (
  data: Buffer,
  encryptionKey: string
): IENetPacket | null => {
  const needsEncryption = data.length >= MINIMUM_ENCRYPTED_LENGTH;

  if (needsEncryption) {
    const encryptedData = blowfishEncrypt(
      data,
      Buffer.from(encryptionKey, "base64")
    );

    return enet.packet.create(encryptedData);
  }

  return enet.packet.create(data);
};

const sendPacket = (
  channel: Channel,
  data: Buffer,
  encryptionKey: string,
  peer: IENetPeer
): void => {
  const packet = createPacket(data, encryptionKey);

  if (packet === null) {
    console.error("Unable to create packet", {
      channel,
      data: data.toString("hex"),
    });
  } else {
    enet.peer.send(peer, channel, packet);
  }
};

const send = (
  channel: Channel,
  data: StructObjectBase,
  player: IPlayerState
): void => {
  const serialized = data.ref();
  console.log(`<< [${Channel[channel].toUpperCase()}]`, {
    command: Command[serialized.readUInt8(0)],
  });

  if (player.peer) {
    sendPacket(channel, serialized, player.encryptionKey, player.peer);
  }
};

const broadcast = (
  channel: Channel,
  data: StructObjectBase,
  players: IPlayerState[]
): void => {
  const serialized = data.ref();
  console.log(`(( [${Channel[channel].toUpperCase()}]`, {
    command: Command[serialized.readUInt8(0)],
  });

  players.forEach((player): void => {
    if (player.peer) {
      sendPacket(channel, serialized, player.encryptionKey, player.peer);
    }
  });
};

const broadcastTeam = (
  channel: Channel,
  data: StructObjectBase,
  players: IPlayerState[],
  team: Team
): void => {
  broadcast(
    channel,
    data,
    players.filter((player) => player.teamId === team)
  );
};

const getDataUnencrypted = (packet: IENetPacket): Buffer => {
  return packet.data;
};

const getData = (packet: IENetPacket, encryptionKey: string): Buffer => {
  return blowfishDecrypt(packet.data, Buffer.from(encryptionKey, "base64"));
};

export { broadcast, broadcastTeam, getData, getDataUnencrypted, send };
