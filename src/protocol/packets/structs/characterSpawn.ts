import array from "ref-array-di";
import ref from "ref-napi";
import struct from "ref-struct-di";

const arrayType = array(ref);
const structType = struct(ref);

const maxPlayerNameLength = 128;
const characterNameLength = 40;
export const characterSpawn = structType(
  [
    [ref.types.byte, "command"],
    [ref.types.uint, "netId"],
    [ref.types.uint, "netId2"],
    [ref.types.int, "clientId"],
    [ref.types.byte, "netNodeId"],
    [ref.types.byte, "botSkillLevel"],
    [ref.types.byte, "teamNumber"],
    [ref.types.byte, "isBot"],
    [ref.types.byte, "spawnPosIndex"],
    [ref.types.int, "skinId"],
    [arrayType(ref.types.char, maxPlayerNameLength), "playerName"],
    [arrayType(ref.types.char, characterNameLength), "characterName"],
    [ref.types.float, "deathDurationRemaining"],
    [ref.types.float, "timeSinceDeath"],
    [ref.types.int, "unk1"],
    [ref.types.byte, "unk2"],
  ],
  { packed: true }
);
