import array from "ref-array-di";
import ref from "ref-napi";
import struct from "ref-struct-di";

const arrayType = array(ref);
const structType = struct(ref);

const paramsLength = 32;
export const chatMessage = structType(
  [
    [ref.types.byte, "command"],
    [ref.types.int, "clientId"],
    [ref.types.int, "botNetId"],
    [ref.types.byte, "isBotMessage"],
    [ref.types.int, "channel"],
    [ref.types.int, "paramsLength"],
    [ref.types.int, "textLength"],
    [arrayType(ref.types.byte, paramsLength), "params"],
  ],
  { packed: true }
);
