import array from "ref-array-di";
import ref from "ref-napi";
import struct from "ref-struct-di";

const arrayType = array(ref);
const structType = struct(ref);

const item = structType(
  [
    [ref.types.uint, "itemId"],
    [ref.types.byte, "slot"],
    [ref.types.byte, "itemsInSlot"],
    [ref.types.byte, "spellCharges"],
  ],
  { packed: true }
);

const shield = structType(
  [
    [ref.types.float, "magical"],
    [ref.types.float, "physical"],
    [ref.types.float, "magicalAndPhysical"],
  ],
  { packed: true }
);

const stack = structType(
  [
    [ref.types.int, "skinNameLength"],
    [arrayType(ref.types.char), "skinName"],
    [ref.types.uint, "skinId"],
    [ref.types.byte, "bitField"],
    [ref.types.uint, "id"],
  ],
  { packed: true }
);

const buff = structType(
  [
    [ref.types.byte, "slot"],
    [ref.types.int, "count"],
  ],
  { packed: true }
);

export const enterVisibilityClient = structType(
  [
    [ref.types.byte, "command"],
    [ref.types.uint, "netId"],
    [ref.types.ushort, "unk1"],
    [ref.types.byte, "itemsLength"],
    [arrayType(item), "items"],
    [ref.types.bool, "hasShield"],
    [shield, "shield"],
    [ref.types.int, "stackLength"],
    [arrayType(stack), "stack"],
    [ref.types.uint, "lookAtNetId"],
    [ref.types.byte, "lookAtType"],
    [ref.types.float, "x"],
    [ref.types.float, "y"],
    [ref.types.float, "z"],
    [ref.types.int, "buffsLength"],
    [buff, "buffs"],
    [ref.types.bool, "unk2"],
  ],
  { packed: true }
);
