import array from "ref-array-di";
import ref from "ref-napi";
import struct from "ref-struct-di";

const arrayType = array(ref);
const structType = struct(ref);

const characterNameLength = 16;
export const loadScreenPlayerCharacter = structType(
  [
    [ref.types.byte, "command"],
    [ref.types.int64, "playerId"],
    [ref.types.int, "skinId"],
    [ref.types.int, "length"],
    [arrayType(ref.types.char, characterNameLength), "name"],
    [ref.types.byte, "unk1"],
  ],
  { packed: true }
);
