import ref from "ref-napi";
import struct from "ref-struct-di";

const structType = struct(ref);

export const pingLoadInfo = structType(
  [
    [ref.types.byte, "command"],
    [ref.types.uint, "netId"],
    [ref.types.int, "clientId"],
    [ref.types.int64, "playerId"],
    [ref.types.float, "loadedPercentage"],
    [ref.types.float, "secondsRemaining"],
    [ref.types.short, "secondsElapsed"],
    [ref.types.short, "ping"],
    [ref.types.byte, "ready"],
  ],
  { packed: true }
);
