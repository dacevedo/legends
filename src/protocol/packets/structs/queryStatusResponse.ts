import ref from "ref-napi";
import struct from "ref-struct-di";

const structType = struct(ref);

export const queryStatusResponse = structType(
  [
    [ref.types.byte, "command"],
    [ref.types.int, "netId"],
    [ref.types.byte, "gameStatus"],
  ],
  { packed: true }
);
