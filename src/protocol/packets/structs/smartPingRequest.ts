import ref from "ref-napi";
import struct from "ref-struct-di";

const structType = struct(ref);

export const smartPingRequest = structType(
  [
    [ref.types.byte, "command"],
    [ref.types.uint, "netId"],
    [ref.types.float, "x"],
    [ref.types.float, "y"],
    [ref.types.uint, "targetNetId"],
    [ref.types.byte, "type"],
  ],
  { packed: true }
);
