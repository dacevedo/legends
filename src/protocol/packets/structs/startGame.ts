import ref from "ref-napi";
import struct from "ref-struct-di";

const structType = struct(ref);

export const startGame = structType(
  [
    [ref.types.byte, "command"],
    [ref.types.uint, "netId"],
    [ref.types.byte, "enablePause"],
  ],
  { packed: true }
);
