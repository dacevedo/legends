import ref from "ref-napi";
import struct from "ref-struct-di";

const structType = struct(ref);

export const startSpawn = structType(
  [
    [ref.types.byte, "command"],
    [ref.types.uint, "netId"],
  ],
  { packed: true }
);
