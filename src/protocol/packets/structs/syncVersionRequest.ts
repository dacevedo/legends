import array from "ref-array-di";
import ref from "ref-napi";
import struct from "ref-struct-di";

const arrayType = array(ref);
const structType = struct(ref);

const versionLength = 256;
export const syncVersionRequest = structType(
  [
    [ref.types.byte, "command"],
    [ref.types.int, "netId"],
    [ref.types.int, "clientId"],
    [arrayType(ref.types.char, versionLength), "version"],
  ],
  { packed: true }
);
