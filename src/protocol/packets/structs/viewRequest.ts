import ref from "ref-napi";
import struct from "ref-struct-di";

const structType = struct(ref);

const vector3 = structType(
  [
    [ref.types.float, "x"],
    [ref.types.float, "y"],
    [ref.types.float, "z"],
  ],
  { packed: true }
);

export const viewRequest = structType(
  [
    [ref.types.byte, "command"],
    [ref.types.uint, "netId"],
    [vector3, "cameraPosition"],
    [vector3, "cameraDirection"],
    [ref.types.int, "clientId"],
    [ref.types.byte, "syncId"],
  ],
  { packed: true }
);
