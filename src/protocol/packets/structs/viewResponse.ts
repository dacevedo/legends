import ref from "ref-napi";
import struct from "ref-struct-di";

const structType = struct(ref);

export const viewResponse = structType(
  [
    [ref.types.byte, "command"],
    [ref.types.byte, "syncId"],
  ],
  { packed: true }
);
