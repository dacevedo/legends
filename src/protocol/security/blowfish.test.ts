import { blowfishDecrypt, blowfishEncrypt } from "./blowfish";

describe("blowfish cipher", () => {
  it("should encrypt with blowfish", () => {
    expect.hasAssertions();

    const key = Buffer.from("test1234", "utf8");
    const plainText = Buffer.from("Hello world!", "utf8");
    const encrypted = blowfishEncrypt(plainText, key);

    expect(encrypted.toString("hex")).toBe("3829e1d29c3cf3214f98c76c2b85374e");
  });

  it("should decrypt with blowfish", () => {
    expect.hasAssertions();

    const key = Buffer.from("test1234", "utf8");
    const encrypted = Buffer.from("3829e1d29c3cf3214f98c76c2b85374e", "hex");
    const decrypted = blowfishDecrypt(encrypted, key);

    expect(decrypted.toString("hex")).toBe("48656c6c6f20776f726c642104040404");
  });
});
