import { base64Decode, base64Encode } from "./encoding";

describe("encoding utility", () => {
  it("should encode text to base64", () => {
    expect.hasAssertions();

    const plainText = "Hello World!";
    const encoded = base64Encode(plainText);

    expect(encoded).toBe("SGVsbG8gV29ybGQh");
  });

  it("should decode base64 to text", () => {
    expect.hasAssertions();

    const encoded = "SGVsbG8gV29ybGQh";
    const plainText = base64Decode(encoded);

    expect(plainText).toBe("Hello World!");
  });
});
