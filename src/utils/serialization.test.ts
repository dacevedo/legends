import type { TypedArray } from "ref-array-di";

import { serializeArray, serializeString } from "./serialization";

describe("serialization utility", () => {
  it("should serialize array", () => {
    expect.hasAssertions();

    const input = [0, 1, 2, 4];
    const serialized: TypedArray<number, 4> = serializeArray(input);

    expect(serialized).toBe(input);
    expect(Array.isArray(serialized)).toBe(true);
  });

  it("should serialize string", () => {
    expect.hasAssertions();

    const input = "Hello!";
    const serialized: TypedArray<number> = serializeString(input);

    expect(serialized).toStrictEqual(["H", "e", "l", "l", "o", "!"]);
    expect(Array.isArray(serialized)).toBe(true);
  });
});
