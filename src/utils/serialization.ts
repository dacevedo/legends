import type { TypedArray } from "ref-array-di";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const serializeArray = <T extends TypedArray<any, any>>(
  array: unknown[]
): T => array as unknown as T;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const serializeString = <T extends TypedArray<number, any>>(
  string: string
): T => serializeArray<T>(Array.from(string));
